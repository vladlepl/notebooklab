﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookLab
{
    class Program
    {
        public static List<Note> notes = new List<Note>();

        // прости меня, макаронный монстр, за такое, вот бы регулярочки...
        static string ruAlphabetLower = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        static string ruAlphabetFull = ruAlphabetLower + ruAlphabetLower.ToUpper();

        static string enAlphabetLower = "abcdefghijklmnopqrstuvwxyz";
        static string enAlphabetFull = enAlphabetLower + enAlphabetLower.ToUpper();

        static string digits = "0123456789";

        // изначально планировалось слово из фильма "Евротур"
        // но если Вы его не смотрели, то при гуглении этого слова
        // Вы бы словили эксепшОн головного мозга (поэтому была взята lite-версия)
        static string stopWord = "АСТАНАВИТЕСЬ!";

        static void Main(string[] args)
        {
            notes.Add(new Note("а", "а", "а", "1", "а", DateTime.Now, "а", "а", "а"));
            notes.Add(new Note("б", "б", "б", "1", "б", DateTime.Now, "б", "б", "б"));
            notes.Add(new Note("в", "в", "в", "1", "в", DateTime.Now, "в", "в", "в"));



            Console.WriteLine("ДИСКЛЕЙМЕР!");
            Console.WriteLine("Произойдет очень специфичный юмор, если при создании новой записи прекратить мучения (ввести стоп-слово \"АСТАНАВИТЕСЬ!\") на поле Номер телефона.");
            Console.WriteLine("Если что, я предупредил!!!");
            Console.WriteLine();

            bool isExitRequired = false;

            do
            {
                WriteInitWords();

                switch (Console.ReadLine())
                {
                    case "1":
                        if(CreateNote())
                            Console.WriteLine("Запись успешно добавлена!");
                        else
                            Console.WriteLine("Создание записи было отменено!");
                        break;
                    case "2":
                        if(EditNote())
                            Console.WriteLine("Редактирование прошло успешно!");
                        else
                            Console.WriteLine("Не удалось изменить запись!");
                        break;
                    case "3":
                        if (RemoveNote())
                            Console.WriteLine("Запись удалена!");
                        else
                            Console.WriteLine("Не удалось удалить запись!");
                        break;
                    case "4":
                        ShowNote();
                        break;
                    case "5":
                        ShowAllNotes();
                        break;
                    default:
                        Console.WriteLine("Вы действительно хотите выйти (1 - Выход, другое - Отмена)?");
                        if (Console.ReadLine() == "1")
                        {
                            Console.WriteLine("Ну ты там это, заходи если что!");
                            isExitRequired = true;
                        }
                        break;
                }
                    
                Console.WriteLine("Нажмите любую клавишу, чтобы продолжить...");
                Console.ReadKey();
                Console.Clear();
            }
            while (!isExitRequired);
        }

        private static void WriteInitWords()
        {
            Console.WriteLine("===== Записная книжка =====");
            Console.WriteLine("Что Вы хотите сделать?");
            Console.WriteLine("1. Создать новую запись.");
            Console.WriteLine("2. Редактировать запись.");
            Console.WriteLine("3. Удалить запись.");
            Console.WriteLine("4. Просмотр записи.");
            Console.WriteLine("5. Просмотр всех записей.");
            Console.WriteLine("другое - Выход.");
        }

        public static bool CreateNote()
        {
            Console.Clear();
            Console.WriteLine("== Создание новой записи ==");

            string firstName = InputNecessary("Введите имя (обязательное поле): ", ruAlphabetFull + " ");
            // прости меня, макаронный монстр, за такое, вот бы исключения пробрасывать...
            if (firstName == null)
                return false;

            string secondName = InputNecessary("Введите фамилию (обязательное поле): ", ruAlphabetFull + " ");
            if (secondName == null)
                return false;

            string middleName = InputUnnecessary("Введите отчество (необязательное поле): ", ruAlphabetFull + " ");
            string phoneNumber = InputNecessary("Введите номер телефона (обязательное поле): ", digits);
            if (phoneNumber == null)
            {
                Console.Clear();
                Console.WriteLine("Поздравляю, Вы открыли бонусный уровень!");
                Console.WriteLine("Вы смотрели видео \"Путин и Медведев, че там с деньгами?\" (1 - Да, другое - Нет)");
                Console.WriteLine("Дело в том, что в ходе проведения тестирования этой программы на людях выяснилось, что не все знакомы с этим великолепным диалогом, и чтобы меня не посчитали сумасшедшим, советую не выбирать ответ Да, если не видели видео!");
                Console.Write("Ваш ответ: ");

                if (Console.ReadLine() != "1")
                {
                    Console.WriteLine("Иди своей дорогой, сталкер!");
                    return false;
                }

                // Я, конечно, понимаю, что юмор, возможно, так себе
                // но я просмеялся, пока это делал
                // если что - извините)
                // (надеюсь, на курсе не предусмотрено отчисление за плохой юмор=) )

                Console.WriteLine("— Да?");
                Console.WriteLine("— Алё!");
                Console.WriteLine("— Да да?");
                Console.WriteLine("— Ну как там с лабой?");
                Console.WriteLine("— А?");
                Console.WriteLine("— Как с лабой - то там?");
                Console.WriteLine("— Чё с лабой?");
                Console.WriteLine("— Чё?");
                Console.WriteLine("— Куда ты звонишь?");
                Console.WriteLine("— Тебе звоню.");
                Console.WriteLine("— Кому?");
                Console.WriteLine("— Ну тебе.");
                Console.WriteLine("— Кому тебе?");
                Console.WriteLine("— А вот тебе вот.");
                Console.WriteLine("— Ты кто такой?");
                Console.WriteLine("— Плахотнюк Владлен.");
                Console.WriteLine("— Какой Плахотнюк Владлен?");
                Console.WriteLine("— Владлен.");
                Console.WriteLine("— Такого не знаю, ты ошибся номером, друг.");
                Console.WriteLine("— Кто?");
                Console.WriteLine("— Ты.");
                Console.WriteLine("— Чё с лабой?");
                Console.WriteLine("— Какой лабой?");
                Console.WriteLine("— Ну которую я скинул.");
                Console.WriteLine("— Куда?");
                Console.WriteLine("— На почту.");
                Console.WriteLine("— Ты пьяный или кто, сынок?");
                Console.WriteLine("— Я тре..Я Плахотнюк Владлен Александрович.");
                Console.WriteLine("— Кто такой?");
                Console.WriteLine("— Пьяный.");
                Console.WriteLine("— Вот именно, ну и все, завяжи лямку.");
                Console.WriteLine("— Куда... Чё завязать?");
                Console.WriteLine("— Завяжи лямку!");
                Console.WriteLine("— А чё ты кричишь?");
                Console.WriteLine("— Да ничего!");
                Console.WriteLine("— Алё!");
                Console.WriteLine("— Да.");
                Console.WriteLine("— Ну чё там?");
                Console.WriteLine("— Чего?!");
                Console.WriteLine("— Как с лабой обстоят во... Обстоит вопрос?");
                Console.WriteLine("— С какой лабой?");
                Console.WriteLine("— Которую я скинул.");
                Console.WriteLine("— Кого скинул?");
                Console.WriteLine("— Лабу первую.");
                Console.WriteLine("— Какую?");
                Console.WriteLine("— В смысле какую?");
                Console.WriteLine("— Ты куда звонишь?! По какому номеру звонишь?");
                Console.WriteLine("— По твоему номеру.");
                Console.WriteLine("— Ты, сынок!");
                Console.WriteLine("— Чё?!");
                Console.WriteLine("— Сынок!");
                Console.WriteLine("— Чё?");
                Console.WriteLine("— Ты куда звонишь?!");
                Console.WriteLine("— Чё ты кричишь?");
                Console.WriteLine("— По какому телефону звонишь?!");
                Console.WriteLine("— Двадцать два, пятьсот пять.");
                Console.WriteLine("— Ну? Какая лаба? Студент ты несчастный!");
                Console.WriteLine("— Пер... Первая лаба!");
                Console.WriteLine("— Ну, первая.");
                Console.WriteLine("— Ну а чё ты кричишь?");
                Console.WriteLine("— Да ничего! А то тесты не пройдет лаба, студент несчастный!");
                Console.WriteLine("— Ты чё?");
                Console.WriteLine("— Ты понял, да?!");
                Console.WriteLine("— Я не понимаю куд...");
                Console.WriteLine("— Всё затнись!");
                Console.WriteLine("— Алё.");
                Console.WriteLine("— Студент ты несчастный!");
                Console.WriteLine("— А чё я?");
                Console.WriteLine("— Ты приедь сюда, я тебе тесты провалю!");
                Console.WriteLine("— А какой адрес?");
                Console.WriteLine("— Вот приедь сюда, я тебе тесты провалю, студент несчастный!");
                Console.WriteLine("— Да не надо.");
                Console.WriteLine("— Ты понял, да?!");
                Console.WriteLine("— За что?");
                Console.WriteLine("— За всё!");
                Console.WriteLine("— А чё я сделал?");
                Console.WriteLine("— Студент несчастный!");
                Console.WriteLine("— Чё с лабой?");
                Console.WriteLine("— Ты понял меня?!");
                Console.WriteLine("— Чё с лабой, я спрашиваю?");
                Console.WriteLine("— Вот всё, какие то лабы, студент несчастный.");
                Console.WriteLine("— Кто?");
                Console.WriteLine("— Ты!");
                Console.WriteLine("— Почему?");
                Console.WriteLine("— Всё, заткнись.");
                Console.WriteLine("— Алё.Алё.");
                Console.WriteLine("— Я щас ещё узнаю с какого ты телефона звонишь!");
                Console.WriteLine("— Я звоню щас из самоизоляции.");
                Console.WriteLine("— С какой самоизоляции?");
                Console.WriteLine("— Находящейся по улице Ломоносова, девять.");
                Console.WriteLine("— Вот, номер твой телефон скажи мне.");
                Console.WriteLine("— Алё.");
                Console.WriteLine("— Скажи номер телефона своего.");
                Console.WriteLine("— Зачем?");
                Console.WriteLine("— А чтобы я приехал тебе тесты провалил.");
                Console.WriteLine("— За что это?");
                Console.WriteLine("— А за всё! Ты понял, да?");
                Console.WriteLine("— Ничё не понял.");
                Console.WriteLine("— Я тебе сказал, сынок.");
                Console.WriteLine("— Что?");
                Console.WriteLine("— Ты понял меня?");
                Console.WriteLine("— Папа!");
                Console.WriteLine("— Всё.");
                Console.WriteLine("— Чё всё то?");
                Console.WriteLine("— Ты давай номер телефона или сюда приезжай.");
                Console.WriteLine("— А с лабой как вопрос обстоит?");
                Console.WriteLine("— С какой лабой?!");
                Console.WriteLine("— Которую я скинул.");
                Console.WriteLine("— Куда скинул?!");
                Console.WriteLine("— На почту.");
                Console.WriteLine("— На какую?");
                Console.WriteLine("— Ну alexander.isaev@firstlinesoftware.com.");
                Console.WriteLine("— Студент несчастный!");
                Console.WriteLine("— Что?");
                
                return false;
            }

            string country = InputUnnecessary("Введите страну (необязательное поле): ", ruAlphabetFull + " ");

            // ну тут вообще no comments
            // надеюсь, этот код никто смотреть не будет
            DateTime birthDate = DateTime.MinValue;
            do
            {
                string value = InputUnnecessary("Введите дату рождения в формате ДЕНЬ.МЕСЯЦ.ГОД (необязательное поле): ", digits + ".");
                if (value != null && value != string.Empty)
                {
                    if (!DateTime.TryParse(value, out birthDate))
                    {
                        Console.WriteLine("Неверный формат даты! Попробуйте ещё раз!");
                        continue;
                    }
                    else
                    {
                        var yearDiff = (DateTime.Now - birthDate).TotalDays / 365.25;
                        if (yearDiff > 100)
                        {
                            Console.WriteLine("Конфуций, разлогинься!");
                            birthDate = DateTime.MinValue;
                        }
                        else if (yearDiff < 0)
                        {
                            Console.WriteLine("*звонит в зону 51*");
                            birthDate = DateTime.MinValue;
                        }
                    }
                }

                break;
            }
            while (true);

            string organization = InputUnnecessary("Введите название организации (необязательное поле): ", null);
            string position = InputUnnecessary("Введите должность (необязательное поле): ", null);
            string otherNotes = InputUnnecessary("Введите заметки (необязательное поле): ", null);

            notes.Add(new Note(firstName, middleName, secondName, phoneNumber, country, birthDate, organization, position, otherNotes));

            return true;
        }

        // Предисловие:
        // Если Вы это читаете, то можете не читать)
        // Просто я попытался собрать некоторые свои мысли по поводу того ужаса, что таится ниже
        // может, через года я вернусь к этому коду и скажу
        // "даааа, а вот почему я тогда не сделал так и так?"


        // мысли вслух...
        // За этот метод вообще стыдно
        // По идее, логика валидации должна быть в классе Note
        // но я не хочу туда выносить логику работы с консольным вводом-выводом

        // можно было бы сделать в Note статический метод а-ля Validate(string value...)
        // который возвращал бы bool (наверно... либо код результата операции через enum, либо вообще Exception'ами бросался бы)
        // но тогда как определять, какое свойство проходит валидацию?
        // писать для каждого свойства метод Validate - перебор, как по мне
        // либо принимать в Validate название свойства или тот же элемент enum'а (Name, Surname, BirthDate и т.д.)
        // но и это выглядит слишком костыльно
        // так как метод будет просто нереальных размеров (впрочем, как и тот, что ниже=) )

        // хотя, если бы я так не заморачивался с вводом данных 
        // (все поля по порядку, обязательные поля в бесконечном цикле и т.д.)
        // то можно было бы просто говорить, мол, сорян, ты ввел ерунду
        // пытайся создать запись заново

        // или вообще валидировать все данные в один момент
        // но тогда, если пользователь ошибся уже при вводе имени
        // ему все равно будет необходимо заполнить остальные данные и в конце он получит ошибку
        // "Простите, имя неправильное!"....

        // ещё и Unit-тесты было бы неплохо прикрутить...
        // тогда еще на моменте создания экземпляра класса Note должна вылезать ошибка
        // вылезти она может только через проброс Exception'а
        // но я стараюсь сделать эту лабу без них (без эксепшОнов)...

        
        // короче, вывод такой
        // наверное, наилучший вариант - проброс Exception'а на моменте создания экземлпяра Note
        // но тогда никаких тебе 100% заполнений обязательных полей
        // (ну либо реально по методу валидации на каждое свойство...)
        
        // или класс-валидатор... ведь модель (Note), скорее всего, будет оставаться неизменной
        // а правила валидации могут меняться (хотя тоже спорно)...

        // КОНЕЦ

        public static bool EditNote()
        {
            Console.Clear();
            Console.WriteLine("== Редактирование записи ==");
            Console.WriteLine("Введите Id записи");

            string input = Console.ReadLine();
            if (int.TryParse(input, out int id))
            {
                Note requestedNote = GetNoteById(id);

                if (requestedNote != null)
                {
                    Console.WriteLine("Что Вы хотите редактировать?");
                    Console.WriteLine("1. Имя");
                    Console.WriteLine("2. Фамилию");
                    Console.WriteLine("3. Отчество");
                    Console.WriteLine("4. Номер телефона");
                    Console.WriteLine("5. Страну");
                    Console.WriteLine("6. Дату рождения");
                    Console.WriteLine("7. Организацию");
                    Console.WriteLine("8. Должность");
                    Console.WriteLine("9. Прочие заметки");
                    Console.WriteLine("другое. Отмена");

                    switch (Console.ReadLine())
                    {
                        case "1":
                            Console.WriteLine($"Предыдущее имя: {requestedNote.FirstName}");
                            string firstName = InputNecessary("Введите новое имя (обязательное поле): ", ruAlphabetFull + " ");

                            if (firstName == null)
                                return false;
                            else
                                requestedNote.FirstName = firstName;
                            return true;
                        case "2":
                            Console.WriteLine($"Предыдущая фамилия: {requestedNote.SecondName}");
                            string secondName = InputNecessary("Введите новую фамилию (обязательное поле): ", ruAlphabetFull + " ");

                            if (secondName == null)
                                return false;
                            else
                                requestedNote.SecondName = secondName;
                            return true;
                        case "3":
                            Console.WriteLine($"Предыдущее отчество: {requestedNote.MiddleName}");
                            requestedNote.MiddleName = InputUnnecessary("Введите новое отчество (необязательное поле): ", ruAlphabetFull + " ");
                            return true;
                        case "4":
                            Console.WriteLine($"Предыдущий номер телефона: {requestedNote.PhoneNumber}");
                            string phoneNumber = InputNecessary("Введите новый номер телефона (обязательное поле): ", digits);

                            if (phoneNumber == null)
                                return false;
                            else
                                requestedNote.PhoneNumber = phoneNumber;
                            return true;
                        case "5":
                            Console.WriteLine($"Предыдущая страна: {(string.IsNullOrEmpty(requestedNote.Country) ? "отсутствует" : requestedNote.Country)}");
                            requestedNote.Country = InputUnnecessary("Введите новую страну (необязательное поле): ", ruAlphabetFull + " ");
                            return true;
                        case "6":
                            Console.WriteLine($"Предыдущая дата рождения: {(requestedNote.BirthDate != DateTime.MinValue ? requestedNote.BirthDate.ToString() : "отсутствует")}");
                            DateTime birthDate = DateTime.MinValue;
                            do
                            {
                                string value = InputUnnecessary("Введите новую дату рождения в формате ДЕНЬ.МЕСЯЦ.ГОД (необязательное поле): ", digits + ".");
                                if (value != null && value != string.Empty)
                                {
                                    if (!DateTime.TryParse(value, out birthDate))
                                    {
                                        Console.WriteLine("Неверный формат даты! Попробуйте ещё раз!");
                                        continue;
                                    }
                                    else
                                    {
                                        var yearDiff = (DateTime.Now - birthDate).TotalDays / 365.25;
                                        if (yearDiff > 100)
                                        {
                                            Console.WriteLine("Конфуций, разлогинься!");
                                            birthDate = DateTime.MinValue;
                                        }
                                        else if (yearDiff < 0)
                                        {
                                            Console.WriteLine("*звонит в зону 51*");
                                            birthDate = DateTime.MinValue;
                                        }
                                    }
                                }

                                break;
                            }
                            while (true);

                            requestedNote.BirthDate = birthDate;
                            return true;
                        case "7":
                            Console.WriteLine($"Предыдущая организация: {(string.IsNullOrEmpty(requestedNote.Organization) ? "отсутствует" : requestedNote.Organization)}");
                            requestedNote.Organization = InputUnnecessary("Введите новое название организации (необязательное поле): ", null);
                            return true;
                        case "8":
                            Console.WriteLine($"Предыдущая должность: {(string.IsNullOrEmpty(requestedNote.Position) ? "отсутствует" : requestedNote.Position)}");
                            requestedNote.Position = InputUnnecessary("Введите новую должность (необязательное поле): ", null);
                            return true;
                        case "9":
                            Console.WriteLine($"Предыдущие заметки: {(string.IsNullOrEmpty(requestedNote.OtherNotes) ? "отсутствует" : requestedNote.OtherNotes)}");
                            requestedNote.OtherNotes = InputUnnecessary("Введите новые заметки (необязательное поле): ", null);
                            return true;
                        default:
                            break;
                    }
                }
                else
                    Console.WriteLine("Запись с таким Id не найдена!");
            }
            else
                Console.WriteLine("Необходимо ввести число!");

            return false;
        }

        public static bool RemoveNote()
        {
            Console.Clear();
            Console.WriteLine("== Удаление записи ==");
            Console.WriteLine("Введите Id записи");

            string input = Console.ReadLine();
            if (int.TryParse(input, out int id))
            {
                Note requestedNote = GetNoteById(id);

                if (requestedNote != null)
                {
                    notes.Remove(requestedNote);
                    return true;
                }
                else
                    Console.WriteLine("Запись с таким Id не найдена!");
            }
            else
                Console.WriteLine("Необходимо ввести число!");

            return false;
        }

        public static void ShowNote()
        {
            Console.Clear();
            Console.WriteLine("== Просмотр записи ==");
            Console.WriteLine("Введите Id записи");

            string input = Console.ReadLine();
            if (int.TryParse(input, out int id))
            {
                Note requestedNote = GetNoteById(id);

                if (requestedNote != null)
                {
                    Console.WriteLine();
                    Console.WriteLine("Запись найдена!");
                    Console.WriteLine(requestedNote);
                }
                else
                    Console.WriteLine("Запись с таким Id не найдена!");
            }
            else
                Console.WriteLine("Необходимо ввести число!");
        }

        public static void ShowAllNotes()
        {
            Console.Clear();
            Console.WriteLine("== Просмотр записей ==");

            if (notes.Count == 0)
                Console.WriteLine("Записей нет!");
            else
                foreach (var note in notes)
                    Console.WriteLine(note.ToShortString());

            Console.WriteLine("== Просмотр записей ==");
        }

        private static Note GetNoteById(int id)
        {
            Note requestedNote = null;
            foreach (var note in notes)
                if (note.Id == id)
                    requestedNote = note;

            return requestedNote;
        }

        private static string InputUnnecessary(string message, string allowedChars)
        {
            string value = null;

            do
            {
                Console.Write(message);
                value = Console.ReadLine().Trim();

                if (value == stopWord)
                    return null;

                if (!string.IsNullOrEmpty(value))
                {
                    if (allowedChars == null || IsStringCorrect(value, allowedChars))
                        break;
                    else
                        Console.WriteLine($"Ошибка! Введенное значение должно содержать только следующие символы: {allowedChars} \nЕсли хотите прекратить мучения - введите \"{stopWord}\"");
                }
                else
                    break;
            }
            while (true);

            return value;
        }

        private static string InputNecessary(string message, string allowedChars)
        {
            string value = null;

            do
            {
                Console.Write(message);
                value = Console.ReadLine().Trim();

                if (value == stopWord)
                    return null;

                if (!string.IsNullOrEmpty(value))
                {
                    if (allowedChars == null || IsStringCorrect(value, allowedChars))
                        break;
                    else
                        Console.WriteLine($"Ошибка! Введенное значение должно содержать только следующие символы: {allowedChars} \nЕсли хотите прекратить мучения - введите \"{stopWord}\"");
                }
                else
                    Console.WriteLine($"Ошибка! Введено пустое значение! \nЕсли хотите прекратить мучения - введите \"{stopWord}\"");
            }
            while (true);

            return value;
        }

        private static bool IsStringCorrect(string value, string allowedChars)
        {
            bool isCorrect = true;

            foreach (var ch in value)
            {
                if (!allowedChars.Contains(ch))
                {
                    isCorrect = false;
                    break;
                }
            }

            return isCorrect;
        }
    }
}
