﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookLab
{
    class Note
    {
        private static int GlobalId = 1;

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string OtherNotes { get; set; }

        public Note(string firstName, string middleName, string secondName, string phoneNumber, string country, DateTime birthDate, string organization, string position, string otherNotes)
        {
            FirstName = firstName;
            SecondName = secondName;
            MiddleName = middleName;
            PhoneNumber = phoneNumber;
            Country = country;
            BirthDate = birthDate;
            Organization = organization;
            Position = position;
            OtherNotes = otherNotes;

            Id = GlobalId++;
        }

        public override string ToString()
        {
            string res = "";

            res += $"{Id}. {SecondName} {FirstName} {MiddleName}\n";

            if (BirthDate != DateTime.MinValue)
                res += $"   Дата рождения: {BirthDate.ToShortDateString()}\n";

            res += $"   Номер телефона: {PhoneNumber}\n";

            if (!string.IsNullOrEmpty(Country))
                res += $"   Страна: {Country}\n";

            if (!string.IsNullOrEmpty(Organization))
                res += $"   Организация: {Organization}\n";

            if (!string.IsNullOrEmpty(Position))
                res += $"   Должность: {Position}\n";

            if (!string.IsNullOrEmpty(Position))
                res += $"   Прочие заметки: {OtherNotes}\n";

            return res;
        }

        public string ToShortString()
        {
            return $"{Id}. {SecondName} {FirstName}, тел.:{PhoneNumber}";
        }
    }
}
